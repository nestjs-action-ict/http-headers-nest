import { ContinuationLocalStorage } from 'asyncctx';
import { v4 as uuidv4 } from 'uuid';

export class RequestContext{
  static cls = new ContinuationLocalStorage<RequestContext>();

  static get currentContext() {
    return this.cls.getContext();
  }

  readonly requestId: number;

  readonly context: any;

  constructor(public readonly req: Request, public readonly res: Response) {

    const context = {
      pid: req.headers['x-findomestic-processid'] || 'newPID',
      sid: req.headers['x-findomestic-sessionid'] || 'newSID',
      cid: req.headers['x-findomestic-conversationid'] || 'newCID',
      rid: req.headers['x-findomestic-requestid'] || 'newRID',
      qid: req.headers['x-findomestic-sequenceid'] || '0',
      elapsed: '',
      id: uuidv4()
    };




    this.context = context;

    this.requestId = context.id;


  }

}
