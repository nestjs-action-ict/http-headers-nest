import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { RequestContextService } from '../request-context/request-context.service';
import { CustomLogger } from './custom-logger';

export const Logger = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const requestContextService: RequestContextService = new RequestContextService();
    const logger: CustomLogger = new CustomLogger(requestContextService, 'PIPPO');
    return logger
  },
);
