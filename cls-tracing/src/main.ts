import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';
import { LoggingInterceptor } from './custom-logger/logging.interceptor';
const moment = require('moment');

async function bootstrap() {

  const ignoreWhenTrue = format((info, opts) => {
    return info.context.name === 'LoggingInterceptor' ? info : false;

  });

  const app = await NestFactory.create(AppModule,
    {
      logger: WinstonModule.createLogger({
        exitOnError: false,

        transports: [
          new transports.Console({
            level: "debug",
            format: format.combine(
              format.colorize(),
              format.timestamp(),
              format.printf(({level, message, timestamp, ...meta}) => {
                  if (meta.context && meta.context.id && meta.context.name !== 'LoggingInterceptor')  {
                  return `${moment().format('dd/MM/yyyy HH:mm:ss')} [${meta.context.id}] ${level} ${meta.context.name} [${meta.context.pid || ''};${meta.context.cid || ''};${meta.context.sid || ''};${meta.context.rid || ''};${meta.context.qid || ''}] - ${message}`;
                }
                return `${timestamp} ${level} - ${message}`;
              })),
          }),
          new transports.File({
            level: 'info',
            filename: '../logs/correlation.log',
            format: format.combine(
              ignoreWhenTrue(),
              format.printf(({level, message, ...meta}) => {

                   let result =  `${moment().format('DD/MM/yyyy HH:mm:ss')} - [${meta.context.id}] [${meta.name} - ${meta.function}] [${meta.method} ${meta.host} ${meta.url}] [${level}] [${meta.context.pid || ''};${meta.context.cid || ''};${meta.context.sid || ''};${meta.context.rid || ''};${meta.context.qid || ''}] - ${message}`;
                    if (meta.elapsed) {
                     result += `[${meta.elapsed}]`;
                   }
                   return result;

              })),
          })
        ], // alert > error > warning > notice > info > debug
      }),
    });
  //app.useLogger(app.get(CustomLogger));
  app.useGlobalInterceptors(new LoggingInterceptor());
  await app.listen(3000);
}
bootstrap();
