import { Injectable } from '@nestjs/common';
import { RequestContextService } from './request-context/request-context.service';
import { CustomLogger } from './custom-logger/custom-logger';

import axios from "axios";

@Injectable()
export class AppService {

  private readonly logger: CustomLogger

  constructor( private readonly requestContextService: RequestContextService) {
    this.logger = new CustomLogger(requestContextService, AppService.name);
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

  async get(url) {
    const requestContextService = new RequestContextService();
    const headers =  {
      'x-findomestic-processid': requestContextService.currentContext.pid,
        'x-findomestic-sessionid': requestContextService.currentContext.sid,
        'x-findomestic-conversationid': requestContextService.currentContext.cid,
        'x-findomestic-requestid': requestContextService.currentContext.rid,
        'x-findomestic-sequenceid': Number(requestContextService.currentContext.qid) + 1,
    }
    return axios.get(url, {headers})
  }

    async getHello(): Promise<any> {


    this.sleep(500); // sleep for 0.5 seconds
    this.logger.log('I\'m in the app service');
    const response = await this.get('http://localhost:3001');
    return response.data;
  }
}
