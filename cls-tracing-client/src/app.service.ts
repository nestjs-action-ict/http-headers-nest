import { Injectable } from '@nestjs/common';
import { RequestContextService } from './request-context/request-context.service';
import { CustomLogger } from './custom-logger/custom-logger';

@Injectable()
export class AppService {

  private readonly logger: CustomLogger

  constructor( private readonly requestContextService: RequestContextService) {
    this.logger = new CustomLogger(requestContextService, AppService.name)
  }



  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

   getHello(): string {


    this.sleep(500); // sleep for 0.5 seconds
    this.logger.log('I\'m in the app service');
    return 'Hello World from second Service';

  }
}
