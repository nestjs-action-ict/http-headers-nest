import { ContinuationLocalStorage } from 'asyncctx';
import { v4 as uuidv4 } from 'uuid';

export class RequestContext{
  static cls = new ContinuationLocalStorage<RequestContext>();

  static get currentContext() {
    return this.cls.getContext();
  }

  readonly requestId: number;

  readonly context: any;

  constructor(public readonly req: Request, public readonly res: Response) {

    const context = {
      pid: req.headers['x-findomestic-processid'],
      sid: req.headers['x-findomestic-sessionid'],
      cid: req.headers['x-findomestic-conversationid'],
      rid: req.headers['x-findomestic-requestid'],
      qid: req.headers['x-findomestic-sequenceid'] || '0',
      elapsed: '',
      id: uuidv4()
    };




    this.context = context;

    this.requestId = context.id;


  }

}
