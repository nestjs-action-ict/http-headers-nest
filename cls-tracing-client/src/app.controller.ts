import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { CustomLogger } from './custom-logger/custom-logger';
import { RequestContextService } from './request-context/request-context.service';

@Controller()
export class AppController {
  private readonly logger: CustomLogger

  constructor(
    private readonly appService: AppService,
    private readonly requestContextService: RequestContextService
    ) {
    this.logger = new CustomLogger(requestContextService, AppController.name)
  }

  @Get()
   getHello2(): string {
    this.appService.sleep(500); // sleep for 0.5 seconds
    this.logger.log("I'm the controller!")
    return this.appService.getHello();
  }
}
