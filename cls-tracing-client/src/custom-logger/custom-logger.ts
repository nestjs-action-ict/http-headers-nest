import { Logger } from '@nestjs/common';
import { RequestContextService } from '../request-context/request-context.service';

export class CustomLogger extends Logger {

  name: string;
  constructor(private readonly requestContextService: RequestContextService, name: string) {
    super();
    this.name = name;
  }

  log(message: any, context?: string): any {
    if (this.requestContextService){
      this.requestContextService.currentContext.name = this.name
    }
    super.log(message, this.requestContextService.currentContext);
  }

  error(message: any, context?: string): any {
    if (this.requestContextService){
      this.requestContextService.currentContext.name = this.name
    }
    super.error(message, this.requestContextService.currentContext);
  }

  warn(message: any, context?: string): any {
    if (this.requestContextService){
      this.requestContextService.currentContext.name = this.name
    }
    super.warn(message, this.requestContextService.currentContext);
  }

  debug(message: any, context?: string): any {
    if (this.requestContextService){
      this.requestContextService.currentContext.name = this.name
    }
    super.debug(message, this.requestContextService.currentContext);
  }

  verbose(message: any, context?: string): any {
    if (this.requestContextService){
      this.requestContextService.currentContext.name = this.name
    }
    super.verbose(message, this.requestContextService.currentContext);
  }
}
